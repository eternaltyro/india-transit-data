# India Transit Data

Transit data (primarily in GTFS format) for various Indian transport
services.

## Agencies Covered
- Delhi Metro (DMRC)

## Making the Static GTFS feed

Just zip the files

```
cd DMRC
zip foo *.txt
```


## Tests

Some tests are run using feedvalidator.py script provided by Google
under the transitfeed repo.

See Validation-results.html file for feedvalidator output
